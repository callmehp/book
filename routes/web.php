<?php

//use App\Http\Controllers\BookragisterController;
use Illuminate\Support\Facades\Route;
use app\Http\Controllers\BookragisterController;
// use app\Http\Controllers\AuthorsController;
use app\Http\Controllers\bookcategorycontroller;
use app\Http\Controllers\booktopiccontroller;
use Illuminate\Support\Facades\Auth;
//use App\Http\Controllers\BookragisterController;



Route::get('/', 'HomeController@index');

Route::get('/admin', function () {
    return view('admin.dashboard');
});

Auth::routes();

//Route::get('/home', 'HomeController@index')->name('home');

Route::get('/clienthome', 'clientHomeController@index')->name('home');

//create for redirect to admin panel using middleware (we have changes in AdminMiddleware,kernel,LoginController files //here auth and admin indicate to folder)
Route::group(['middleware'  => ['auth','admin']], function() {
	// you can use "/admin" instead of "/dashboard"
	Route::get('/dashboard', 'Admin\DashboardController@index');
	// below is used for adding the users.
	Route::get('/role-register','Admin\DashboardController@registered');
	//below route for edit the users detail and update.
	Route::get('/role-edit/{id}','Admin\DashboardController@registeredit');
	//update button route
	Route::put('/role-register-update/{id}','Admin\DashboardController@registerupdate');
	//delete route
	Route::delete('/role-delete/{id}','Admin\DashboardController@registerdelete');
	

});

// Route::view('userdash','userdash');


//======Book Registration=================================
Route::get('book','BookregisterController@index')->name('book.list');
Route::get('book-edit', 'BookregisterController@create')->name('create-book');
Route::get('book-edit/{id}', 'BookregisterController@edit')->name('edit-book');
Route::post('book-update/{id}','BookregisterController@update')->name('update-book');
Route::post('book-update','BookregisterController@store')->name('store-book');
Route::get('book-del/{id}','BookregisterController@destroy')->name('del-book');

//==========Book Category=================================
Route::get('bookcategory','bookcategorycontroller@index')->name('bookcategory.list');
Route::get('bookcategory-edit', 'bookcategorycontroller@create')->name('create-bookcategory');
Route::get('bookcategory-edit/{id}', 'bookcategorycontroller@edit')->name('edit-bookcategory');
Route::post('bookcategory-update/{id}','bookcategorycontroller@update')->name('update-bookcategory');
Route::post('bookcategory-update','bookcategorycontroller@store')->name('store-bookcategory');
Route::get('bookcategory-del/{id}','bookcategorycontroller@destroy')->name('del-bookcategory');

//=============Book Topic=================================
Route::get('booktopic','booktopiccontroller@index')->name('booktopic.list');
Route::get('booktopic-edit', 'booktopiccontroller@create')->name('create-booktopic');
Route::get('booktopic-edit/{id}', 'booktopiccontroller@edit')->name('edit-booktopic');
Route::post('booktopic-update/{id}','booktopiccontroller@update')->name('update-booktopic');
Route::post('booktopic-update','booktopiccontroller@store')->name('store-booktopic');
Route::get('booktopic-del/{id}','booktopiccontroller@destroy')->name('del-booktopic');


//================ Author =================================
Route::get('auhtor','AuthorsController@index')->name('author.list');
Route::get('auhtor-edit', 'AuthorsController@create')->name('create-auhtor');
Route::get('auhtor-edit/{id}', 'AuthorsController@edit')->name('edit-auhtor');
Route::post('auhtor-update/{id}','AuthorsController@update')->name('update-auhtor');
Route::post('auhtor-update','AuthorsController@store')->name('store-auhtor');
Route::get('auhtor-del/{id}','AuthorsController@destroy')->name('del-auhtor');


//Route::resource('authors', AuthorsController::class);