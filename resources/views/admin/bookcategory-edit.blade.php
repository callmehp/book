@extends('layouts.master')

@section('title')
{{ isset($data) ? 'Edit Book' : 'Add Book'}}
@endsection()

@section('content')
			
<div class="container">
	<div class="row">
		<div class="col-md-12"><!-- 12 row -->
			<div class="card">
				<div class="card-header">
					<h3>{{ isset($data) ? 'Edit' : 'Add'}} Book category</h3>
				</div>
				<div class="card-body">
					<form method="post" action="{{ isset($data) ? route('update-bookcategory', ['id' => $data->id]) : route('store-bookcategory') }}" enctype="multipart/form-data">	
						@csrf
						<div class="row">
							<div class="col-lg-6 col-md-12">
								<div class="form-group">                                                
									<input type="text" class="form-control" placeholder="Book Category" id="bookcategory" name="bookcategory" value="{{ isset($data) && isset($data->bookcategory) ? $data->bookcategory : ''}}" />
								</div>
							</div>
						</div>
				
					<button type="Submit" class="btn btn-success">Submit</button>
					<a href="{{route('bookcategory.list')}}" class="btn btn-danger">Cancel</a>
					</form>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>


@endsection()

@section('scripts')


@endsection()