@extends('layouts.master')

@section('title')
{{ isset($data) ? 'Edit Book' : 'Add Book'}}
@endsection()

@section('content')
			
<div class="container">
	<div class="row">
		<div class="col-md-12"><!-- 12 row -->
			<div class="card">
				<div class="card-header">
					<h3>{{ isset($data) ? 'Edit' : 'Add'}} Book</h3>
				</div>
				<div class="card-body">
					<form method="post" action="{{ isset($data) ? route('update-book', ['id' => $data->id]) : route('store-book') }}" enctype="multipart/form-data">	
						@csrf
						<div class="row">
							<div class="col-lg-6 col-md-12">
	
								<div class="form-group">
									<label class="col-form-label" for="bookname">Book Name </label>   
									<input type="text" class="form-control-cus" placeholder="Book Name" id="bookname" name="bookname" value="{{ isset($data) && isset($data->bookname) ? $data->bookname : ''}}" />
								</div>
								<div class="form-group">
									<label  class="col-form-label" for="writtenby">Select Book Author</label>   
									<select class="form-control" name="writtenby">
										<option value="">Select Author</option>
										@foreach($authors as $author)
											<option {{ isset($data) && isset($data->writtenby) && $data->writtenby == $author->id ? 'selected' : '' }} value="{{ $author->id }}">{{$author->name}}</option>
										@endforeach
									</select>
								</div>
								<div class="form-group">
									<label  class="col-form-label" for="category_id">Select Book Category </label> 
									<select class="form-control" name="category_id">
										<option value="">Select Category</option>
										@foreach($categories as $category)
											<option {{ isset($data) && isset($data->category_id) && $data->category_id == $category->id ? 'selected' : '' }} value="{{ $category->id }}">{{$category->bookcategory}}</option>
										@endforeach
									</select>
								</div>
							</div>

							<div class="col-lg-6 col-md-12">
								
								<div class="form-group">
								<label class="col-form-label" for="bookname">Upload File </label>  
									<div class="custom-file">
											<input type="file" class="custom-file-input" id="customFile">
											<label class="custom-file-label" name="filename" for="customFile">Choose PDF file</label>
									</div>
								</div>
								<div class="form-group">
								<label class="col-form-label" for="bookname">Upload Cover Image </label>  
									<div class="custom-file">
											<input type="file" class="custom-file-input" id="customFile">
											<label class="custom-file-label"name="cover" for="customFile">Choose Cover Image</label>
									</div>
								</div>
							</div>
						</div>
						<button type="Submit" class="btn btn-success">Submit</button>
						<a href="{{route('book.list')}}" class="btn btn-danger">Cancel</a>
					</form>
				</div>
			</div>
		</div>
	</div>
</div>


@endsection()

@section('scripts')


@endsection()