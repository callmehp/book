@extends('layouts.master')

@section('title')
		Book Register
@endsection()



@section('content')

<div class="row">
  <div class="col-md-12">
    <div class="card">
      <div class="card-header">
       
         <a href="{{route('create-book')}}" ><button class="btn btn-success" style="float: left;">Add new Book</button></a>

      </div>
    </div>
  </div>
</div> 
<!-- ===================data view ===================================-->
<div class="row">
  <div class="col-lg-12">
    <div class="card">
    
          <div class="card-body">
              <div class="table-responsive">
                  
              <div class="card-body">
                <div class="table-responsive">
                  <table class="table table-hover">
                    <thead class=" text-primary">
                      <!-- fetch table data -->
                      
                        <th>Book Id</th>
                        <th>Book Name</th>
                        <th>Writen By</th>
                        <th>Cover</th>
                        <th>Action</th>
                    </thead>
                    <tbody>
                      <!--fetch table data -->
                      @foreach ($c_rr as $book)
                      <tr>
                        <td>{{$book->id}}</td>
                          <td>{{$book->bookname}}</td>
                          <td>{{$book->author->name}}</td>
                          <td><img class="user-profile-img-custom" src="{{asset('/uploads/'.$book->cover)}}"> </img></td>
                          <td class="actions">
                              <a href="{{route('edit-book', ['id' => $book->id])}}" class="btn btn-success">EDIT</a>
                              <a href="{{route('del-book', ['id' => $book->id])}}" class="btn btn-danger">DELETE</a> 
                             <!-- <a href="delete/{{$book->id}}" class="btn btn-danger">DELETE</a>--> 
                          </td>
                        </tr>
                          @endforeach
                    </tbody>
                   
                  </table>
                  {{ $c_rr->links() }}
              </div>
          </div>
      </div>
  </div>
</div>



@endsection()

@section('scripts')


@endsection()