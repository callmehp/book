@extends('layouts.master')

@section('title')
{{ isset($data) ? 'Edit Book' : 'Add Author'}}
@endsection()

@section('content')
			
<div class="container">
	<div class="row">
		<div class="col-md-12"><!-- 12 row -->
			<div class="card">
				<div class="card-header">
					<h3>{{ isset($data) ? 'Edit' : 'Add'}} Author</h3>
				</div>
				<div class="card-body">
					<form method="post" action="{{ isset($data) ? route('update-auhtor', ['id' => $data->id]) : route('store-auhtor') }}" enctype="multipart/form-data">	
						@csrf
						<div class="row">
							<div class="col-lg-6 col-md-12">
	
								<div class="form-group">                                                
									<input type="text" class="form-control" placeholder="Author Name" id="name" name="name" value="{{ isset($data) && isset($data->name) ? $data->name : ''}}" />
								</div>
								
							</div>
							<div class="col-lg-6 col-md-12">
								<div class="input-group mb-3">
									<div class="custom-file">
										<input type="file" name="image" class="form-control" placeholder="Choose file">
									</div>	
								</div>
								
							</div>
						</div>
					<button type="Submit" class="btn btn-success">Submit</button>
					<a href="{{route('author.list')}}" class="btn btn-danger">Cancel</a>
					</form>
				</div>
			</div>
		</div>
	</div>
</div>


@endsection()

@section('scripts')


@endsection()