@extends('layouts.master')

@section('title')
Author
@endsection()
@section('content')

<div class="row">
  <div class="col-md-12">
    <div class="card">
      <div class="card-header">
        <h4 class="card-title">Authors</h4>
		  <a href="{{route('create-auhtor')}}" ><button class="btn btn-success" style="float: left;">Add Author</button></a>
      </div>
    </div>
  </div>
</div> 
<!-- ===================data view ===================================-->
<div class="row">
  <div class="col-lg-12">
    <div class="card">
        
          <div class="card-body">
              <div class="table-responsive">
                  
              <div class="card-body">
                <div class="table-responsive">
                  <table class="table table-hover">
                    <thead class=" text-primary">
                      <!-- fetch table data -->
                      
                              <th>Author Id</th>
                              <th>Author Name</th>
                              <th>Image</th>
                              <th>Action</th>
                    </thead>
                    <tbody>
                      <!--fetch table data -->
                     @foreach ($data as $author)
                      <tr>
                         <td>{{$author->id}}</td>
                          <td>{{$author->name}}</td>
                          <td><img class="user-profile-img-custom" src="{{asset('/uploads/'.$author->image)}}"> </img></td>
                          <td class="actions">
                              <a href="{{route('edit-auhtor', ['id' => $author->id])}}" class="btn btn-success">EDIT</a>
                        
                              <a href="{{route('del-auhtor', ['id' => $author->id])}}" class="btn btn-danger" onclick="return confirm('Are you sure you want to delete this Category?')">DELETE</a> 
                          </td>
                        </tr>
                          @endforeach
                    </tbody>
                   
                  </table>
              </div>
          </div>
      </div>
  </div>
</div>


<!-- ===================data view ===================================-->
<!-- <div class="row">
  <div class="col-md-12">
    <div class="card">
      <div class="card-header">
        <h4 class="card-title"> Registered User's</h4>
         
      </div>
      <div class="card-body">
        <div class="table-responsive">
          <table class="table">
            <thead class=" text-primary">
            
              <th>Id</th>
              <th>Name</th>
              <th>Phone</th>
              <th>Email</th>
              <th>Usertype</th>
              <th>Edit</th>
              <th>Delete</th>
            </thead>
            <tbody>
           
          
              <tr>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td>
                  <a href="" class="btn btn-success">EDIT</a>
                </td>
                <td>
              
                  <form action="" method="post">
                    {{ csrf_field() }}
                    {{ method_field('DELETE') }}
                    <button type="submit" class="btn btn-danger">DELETE</button> 

                  </form>
                </td>
                </tr>
              
            </tbody>
          </table>
        </div>
      </div>
    </div>
  </div>
</div> -->

@endsection()

@section('scripts')


@endsection()