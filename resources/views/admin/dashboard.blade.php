@extends('layouts.master')

@section('title')
			Welcome Reders.
@endsection()

@section('content')

<div class="row">
          <div class="col-md-12">
          <div class="row clearfix">
                <div class="col-lg-3 col-md-6">
                    <div class="card-custom">
                        <div class="body">
                            <h3 class="number count-to" data-from="0" data-to="128" data-speed="2000" data-fresh-interval="700" >{{ $total->books }}</h3>                        
                            <p class="text-muted">Total Books</p>
                            <div class="progress progress-xs">
                                <div class="progress-bar l-blue" role="progressbar" aria-valuenow="68" aria-valuemin="0" aria-valuemax="100" style="width: 68%;"></div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-lg-3 col-md-6">
                    <div class="card-custom">
                        <div class="body">
                            <h3 class="number count-to" data-from="0" data-to="758" data-speed="2000" data-fresh-interval="700" >{{ $total->authors }}</h3>
                            <p class="text-muted">Total Authors</p>
                            <div class="progress progress-xs">
                                <div class="progress-bar l-green" role="progressbar" aria-valuenow="68" aria-valuemin="0" aria-valuemax="100" style="width: 68%;"></div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-lg-3 col-md-6">
                    <div class="card-custom">
                        <div class="body">
                            <h3 class="number count-to" data-from="0" data-to="2521" data-speed="2000" data-fresh-interval="700" >{{ $total->categories }}</h3>
                            <p class="text-muted">Total Book Categories</p>
                            <div class="progress progress-xs">
                                <div class="progress-bar l-amber" role="progressbar" aria-valuenow="68" aria-valuemin="0" aria-valuemax="100" style="width: 68%;"></div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-lg-3 col-md-6">
                    <div class="card-custom">
                        <div class="body">
                        <h3 class="number count-to" data-from="0" data-to="2521" data-speed="2000" data-fresh-interval="700" >{{ $total->users }}</h3>
                            <p class="text-muted">Total Users</p>
                            <div class="progress progress-xs">
                                <div class="progress-bar l-parpl" role="progressbar" aria-valuenow="68" aria-valuemin="0" aria-valuemax="100" style="width: 68%;"></div>
                            </div>
                        </div>
                    </div>
                </div>            
            </div>
          </div>
        </div>        


        <div class="row clearfix">
            <div class="col-lg-6">
                <div class="card-custom">
                    <div class="header">
                        <h2>Patients Status</h2>
                        <ul class="header-dropdown">
                            <li class="dropdown">
                                <a href="javascript:void(0);" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false"></a>
                                <ul class="dropdown-menu dropdown-menu-right">
                                    <li><a href="javascript:void(0);">Action</a></li>
                                    <li><a href="javascript:void(0);">Another Action</a></li>
                                    <li><a href="javascript:void(0);">Something else</a></li>
                                </ul>
                            </li>
                        </ul>
                    </div>
                    <div class="body">
                      <p class="float-md-right">
                        <span class="badge badge-success">3 Admit</span>
                        <span class="badge badge-default">2 Discharge</span>
                    </p>
                    <div class="table-responsive table_middel">
                        <table class="table m-b-0">
                            <thead class="thead-dark">
                                <tr>
                                    <th>#</th>
                                    <th>Patients</th>
                                    <th>Adress</th>
                                    <th>START Date</th>
                                    <th>END Date</th>
                                    <th>Priority</th>
                                    <th>Progress</th>
                                    <th>Status</th>
                                </tr>
                            </thead>
                            <tbody>
                                <tr>
                                    <td>1</td>
                                    <td><img src="../assets/images/xs/avatar3.jpg" class="rounded-circle width30 m-r-15" alt="profile-image"><span>John</span></td>
                                    <td><span class="text-info">70 Bowman St. South Windsor, CT 06074</span></td>
                                    <td>Sept 13, 2017</td>
                                    <td>Sept 16, 2017</td>
                                    <td><span class="badge badge-warning">MEDIUM</span></td>
                                    <td><div class="progress progress-xs">
                                            <div class="progress-bar progress-bar-warning" role="progressbar" aria-valuenow="40" aria-valuemin="0" aria-valuemax="100" style="width: 40%;"> <span class="sr-only">40% Complete</span> </div>
                                        </div>
                                    </td>
                                    <td><span class="badge badge-success">Admit</span></td>
                                </tr>
                                <tr>
                                    <td>2</td>
                                    <td><img src="../assets/images/xs/avatar1.jpg" class="rounded-circle width30 m-r-15" alt="profile-image"><span>Jack Bird</span></td>
                                    <td><span class="text-info">123 6th St. Melbourne, FL 32904</span></td>
                                    <td>Sept 13, 2017</td>
                                    <td>Sept 22, 2017</td>
                                    <td><span class="badge badge-warning">MEDIUM</span></td>
                                    <td><div class="progress progress-xs">
                                            <div class="progress-bar" role="progressbar" aria-valuenow="100" aria-valuemin="0" aria-valuemax="100" style="width: 100%;"> <span class="sr-only">100% Complete</span> </div>
                                        </div>
                                    </td>
                                    <td><span class="badge badge-default">Discharge</span></td>
                                </tr>
                                <tr>
                                    <td>3</td>
                                    <td><img src="../assets/images/xs/avatar4.jpg" class="rounded-circle width30 m-r-15" alt="profile-image"><span>Dean Otto</span></td>
                                    <td><span class="text-info">123 6th St. Melbourne, FL 32904</span></td>
                                    <td>Sept 13, 2017</td>
                                    <td>Sept 23, 2017</td>
                                    <td><span class="badge badge-warning">MEDIUM</span></td>
                                    <td><div class="progress progress-xs">
                                            <div class="progress-bar progress-bar-success" role="progressbar" aria-valuenow="15" aria-valuemin="0" aria-valuemax="100" style="width: 15%;"> <span class="sr-only">15% Complete</span> </div>
                                        </div>
                                    </td>
                                    <td><span class="badge badge-success">Admit</span></td>
                                </tr>
                                <tr>
                                    <td>4</td>
                                    <td><img src="../assets/images/xs/avatar2.jpg" class="rounded-circle width30 m-r-15" alt="profile-image"><span>Jack Bird</span></td>
                                    <td><span class="text-info">4 Shirley Ave. West Chicago, IL 60185</span></td>
                                    <td>Sept 17, 2017</td>
                                    <td>Sept 16, 2017</td>
                                    <td><span class="badge badge-success">LOW</span></td>
                                    <td><div class="progress progress-xs">
                                            <div class="progress-bar" role="progressbar" aria-valuenow="100" aria-valuemin="0" aria-valuemax="100" style="width: 100%;"> <span class="sr-only">100% Complete</span> </div>
                                        </div>
                                    </td>
                                    <td><span class="badge badge-default">Discharge</span></td>
                                </tr>
                                <tr>
                                    <td>5</td>
                                    <td><img src="../assets/images/xs/avatar5.jpg" class="rounded-circle width30 m-r-15" alt="profile-image"><span>Hughe L.</span></td>
                                    <td><span class="text-info">4 Shirley Ave. West Chicago, IL 60185</span></td>
                                    <td>Sept 18, 2017</td>
                                    <td>Sept 18, 2017</td>
                                    <td><span class="badge badge-danger">HIGH</span></td>
                                    <td><div class="progress progress-xs">
                                            <div class="progress-bar progress-bar-danger" role="progressbar" aria-valuenow="85" aria-valuemin="0" aria-valuemax="100" style="width: 85%;"> <span class="sr-only">85% Complete</span> </div>
                                        </div>
                                    </td>
                                    <td><span class="badge badge-success">Admit</span></td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                    </div>
                </div>
            </div>
            <div class="col-lg-6">
           
                    <div class="card">
                        <div class="header">
                            <h2>Browser Usage</h2>
                            <ul class="header-dropdown">
                                <li class="dropdown">
                                    <a href="javascript:void(0);" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false"></a>
                                    <ul class="dropdown-menu dropdown-menu-right">
                                        <li><a href="javascript:void(0);">Action</a></li>
                                        <li><a href="javascript:void(0);">Another Action</a></li>
                                        <li><a href="javascript:void(0);">Something else</a></li>
                                    </ul>
                                </li>
                            </ul>
                        </div>
                        <div class="body text-center">
                            <div id="donut_chart" class="dashboard-donut-chart m-b-35">
                            <svg height="342" version="1.1" width="280" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" style="overflow: hidden; position: relative; left: -0.25px;"><desc style="-webkit-tap-highlight-color: rgba(0, 0, 0, 0);">Created with Raphaël 2.2.0</desc><defs style="-webkit-tap-highlight-color: rgba(0, 0, 0, 0);"></defs><path fill="none" stroke="#f4b826" d="M140,260.6666666666667A86.66666666666667,86.66666666666667,0,0,0,213.20941455105168,127.61581377017193" stroke-width="2" opacity="1" style="-webkit-tap-highlight-color: rgba(0, 0, 0, 0); opacity: 1;"></path><path fill="#f4b826" stroke="#ffffff" d="M140,263.6666666666667A89.66666666666667,89.66666666666667,0,0,0,215.74358659320347,126.0102073237548L249.81412182657755,104.4237206552579A130,130,0,0,1,140,304Z" stroke-width="3" style="-webkit-tap-highlight-color: rgba(0, 0, 0, 0);"></path><path fill="none" stroke="#009ee8" d="M213.20941455105168,127.61581377017193A86.66666666666667,86.66666666666667,0,0,0,95.75248043121002,99.47974689307168" stroke-width="2" opacity="0" style="-webkit-tap-highlight-color: rgba(0, 0, 0, 0); opacity: 0;"></path><path fill="#009ee8" stroke="#ffffff" d="M215.74358659320347,126.0102073237548A89.66666666666667,89.66666666666667,0,0,0,94.2208355230596,96.90019967013954L76.18146216039906,66.51886571116108A125,125,0,0,1,245.59050175632456,107.09973139928644Z" stroke-width="3" style="-webkit-tap-highlight-color: rgba(0, 0, 0, 0);"></path><path fill="none" stroke="#f49c17" d="M95.75248043121002,99.47974689307168A86.66666666666667,86.66666666666667,0,0,0,64.23125045885678,216.073836336643" stroke-width="2" opacity="0" style="-webkit-tap-highlight-color: rgba(0, 0, 0, 0); opacity: 0;"></path><path fill="#f49c17" stroke="#ffffff" d="M94.2208355230596,96.90019967013954A89.66666666666667,89.66666666666667,0,0,0,61.60848605166336,217.53023836368067L30.71814970027421,234.68341779323512A125,125,0,0,1,76.18146216039906,66.51886571116108Z" stroke-width="3" style="-webkit-tap-highlight-color: rgba(0, 0, 0, 0);"></path><path fill="none" stroke="#f55943" d="M64.23125045885678,216.073836336643A86.66666666666667,86.66666666666667,0,0,0,76.15690843780055,232.61032989918868" stroke-width="2" opacity="0" style="-webkit-tap-highlight-color: rgba(0, 0, 0, 0); opacity: 0;"></path><path fill="#f55943" stroke="#ffffff" d="M61.60848605166336,217.53023836368067A89.66666666666667,89.66666666666667,0,0,0,73.9469552683398,234.63914901108365L47.918617939135416,258.5341296622913A125,125,0,0,1,30.71814970027421,234.68341779323512Z" stroke-width="3" style="-webkit-tap-highlight-color: rgba(0, 0, 0, 0);"></path><path fill="none" stroke="#00c7fe" d="M76.15690843780055,232.61032989918868A86.66666666666667,86.66666666666667,0,0,0,109.88831457694019,255.26744435562006" stroke-width="2" opacity="0" style="-webkit-tap-highlight-color: rgba(0, 0, 0, 0); opacity: 0;"></path><path fill="#00c7fe" stroke="#ffffff" d="M73.9469552683398,234.63914901108365A89.66666666666667,89.66666666666667,0,0,0,108.8459870046035,258.0805481986992L96.56968448597144,291.2126601282981A125,125,0,0,1,47.918617939135416,258.5341296622913Z" stroke-width="3" style="-webkit-tap-highlight-color: rgba(0, 0, 0, 0);"></path><path fill="none" stroke="#565656" d="M109.88831457694019,255.26744435562006A86.66666666666667,86.66666666666667,0,0,0,139.97277286411668,260.66666238983817" stroke-width="2" opacity="0" style="-webkit-tap-highlight-color: rgba(0, 0, 0, 0); opacity: 0;"></path><path fill="#565656" stroke="#ffffff" d="M108.8459870046035,258.0805481986992A89.66666666666667,89.66666666666667,0,0,0,139.9718303863361,263.6666622417941L139.96073009247598,298.9999938314973A125,125,0,0,1,96.56968448597144,291.2126601282981Z" stroke-width="3" style="-webkit-tap-highlight-color: rgba(0, 0, 0, 0);"></path><text x="140" y="164" text-anchor="middle" font-family="&quot;Arial&quot;" font-size="15px" stroke="none" fill="#000000" style="-webkit-tap-highlight-color: rgba(0, 0, 0, 0); text-anchor: middle; font-family: Arial; font-size: 15px; font-weight: 800;" font-weight="800" transform="matrix(1.9697,0,0,1.9697,-135.7576,-169.697)" stroke-width="0.5076923076923077"><tspan dy="6" style="-webkit-tap-highlight-color: rgba(0, 0, 0, 0);">Crome</tspan></text><text x="140" y="184" text-anchor="middle" font-family="&quot;Arial&quot;" font-size="14px" stroke="none" fill="#000000" style="-webkit-tap-highlight-color: rgba(0, 0, 0, 0); text-anchor: middle; font-family: Arial; font-size: 14px;" transform="matrix(1.8056,0,0,1.8056,-112.7778,-141.7778)" stroke-width="0.5538461538461539"><tspan dy="5" style="-webkit-tap-highlight-color: rgba(0, 0, 0, 0);">35%</tspan></text></svg>
                            </div>
                            <div class="row">
                                <div class="col-lg-2 col-4">
                                    <h6>Crome</h6>
                                    <p>35<sup>%</sup></p>
                                </div>
                                <div class="col-lg-2 col-4">
                                    <h6>Safari</h6>
                                    <p>25<sup>%</sup></p>
                                </div>                                
                                <div class="col-lg-2 col-4">
                                    <h6>Mozila</h6>
                                    <p>25<sup>%</sup></p>
                                </div>
                                <div class="col-lg-2 col-4">
                                    <h6>Opera</h6>
                                    <p>3<sup>%</sup></p>
                                </div>
                                <div class="col-lg-2 col-4">
                                    <h6>IE</h6>
                                    <p>7<sup>%</sup></p>
                                </div>
                                <div class="col-lg-2 col-4">
                                    <h6>Others</h6>
                                    <p>5<sup>%</sup></p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            
        </div>
@endsection()

@section('scripts')


@endsection()