@extends('layouts.client')
@include('includes.client-header')
@section('content')

<!--Carousel Wrapper-->
<div id="carousel-example-2" class="carousel slide carousel-fade" data-ride="carousel">
  <!--Indicators-->
  <ol class="carousel-indicators">
    <li data-target="#carousel-example-2" data-slide-to="0" class="active"></li>
    <li data-target="#carousel-example-2" data-slide-to="1"></li>
    <li data-target="#carousel-example-2" data-slide-to="2"></li>
  </ol>
  <!--/.Indicators-->
  <!--Slides-->
  <div class="carousel-inner" role="listbox">
    <div class="carousel-item active">
      <div class="view">
        <img class="d-block w-100" src="https://mdbootstrap.com/img/Photos/Slides/img%20(68).jpg"
          alt="First slide">
        <div class="mask rgba-black-light"></div>
      </div>
      <div class="carousel-caption">
        <h3 class="h3-responsive"><p>Expolore authors, their lifestories, learn philosophy and many more.</p></h3>
        <p>First text</p>
      </div>
    </div>
    <div class="carousel-item">
      <!--Mask color-->
      <div class="view">
        <img class="d-block w-100" src="{{asset('/cliassets/images/page_hero/2.jpg')}}"
          alt="Second slide">
        <div class="mask rgba-black-strong"></div>
      </div>
      <div class="carousel-caption">
        <h3 class="h3-responsive"><p>Expolore authors, their lifestories, learn philosophy and many more.</p></h3>
        <p>Secondary text</p>
      </div>
    </div>
    <div class="carousel-item">
      <!--Mask color-->
      <div class="view">
        <img class="d-block w-100" src="{{asset('/cliassets/images/page_hero/homepage.jpg')}}"
          alt="Third slide">
        <div class="mask rgba-black-slight"></div>
      </div>
      <div class="carousel-caption">
        <h3 class="h3-responsive"></h3>
        <p>Expolore authors, their lifestories, learn philosophy and many more.</p>
      </div>
    </div>
  </div>
  <!--/.Slides-->
  <!--Controls-->
  <a class="carousel-control-prev" href="#carousel-example-2" role="button" data-slide="prev">
    <span class="carousel-control-prev-icon" aria-hidden="true"></span>
    <span class="sr-only">Previous</span>
  </a>
  <a class="carousel-control-next" href="#carousel-example-2" role="button" data-slide="next">
    <span class="carousel-control-next-icon" aria-hidden="true"></span>
    <span class="sr-only">Next</span>
  </a>
  <!--/.Controls-->
</div>
<!--/.Carousel Wrapper-->



<div class="section">
<div class="container">
    @foreach($categories as $category)
        <div class="category-container">
            <div class="row">
                <div class="col-12 d-flex align-items-end justify-content-between">
                    <div class="flex-1">
                        <h3>{{ $category->bookcategory }}</h3>
                    </div>
                    <div class="text-right">
                        <a href="#">See all</a>
                    </div>
                    <!-- /.float-right -->
                </div>
                <!-- /.col-12 -->
            </div>
            <!-- /.row -->
            <div class="row">
                <div class="col-12">
                    <div class="owl-carousel list-carousel">
                        @foreach($books as $book)
                            @if($book->category_id == $category->id)
                            <div class="list-card">
                                <div class="img">
                                    <img src="{{asset('/uploads/'.$book->cover)}}" alt="image alt"/>
                                </div>
                                <!-- /.img -->
                                <div class="info">
                                    <div class="title">
                                        {{$book->bookname}}
                                    </div>
                                    <!-- /.title -->
                                </div>
                                <!-- /.info -->
                            </div>
                            @endif
                        @endforeach
                    </div>
                    <!-- /.list-card owl-carousel list-carousel -->
                </div>
                <!-- /.col-12 -->
            </div>
        </div>
        <!-- /.row -->
    @endforeach
</div>
</div>

<!-- START Login Modal -->
<div id="login-modal" class="modal" tabindex="-1" role="dialog">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content ">
            <div class="modal-body">

                <div class="login-form">
                    <h3 class="title">
                        Log in to continue
                    </h3>
                    <!-- /.title -->
                    <form method="POST" action="{{ route('login') }}">
                        @csrf
                    <div class="form-group">
                        <!--<input type="text" class="form-control form-control-lg" placeholder="Email Address"/>-->
                        <input id="email" type="email" placeholder="Email Address" class="form-control form-control-lg @error('email') is-invalid @enderror" name="email" value="{{ old('email') }}" required autocomplete="email" autofocus>
                        
                        @error('email')
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $message }}</strong>
                        </span>
                    @enderror
                        <!-- /.form-control -->
                    </div>
                    <!-- /.form-group -->

                    <div class="form-group">
                    <input id="password" type="password" placeholder="Password" class="form-control form-control-lg @error('password') is-invalid @enderror" name="password" required autocomplete="current-password">

                    @error('password')
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $message }}</strong>
                        </span>
                    @enderror
                        
                        <!-- /.form-control -->
                    </div>
                    <!-- /.form-group -->
                    <button type="submit" class="mt-4 btn btn-primary btn-block btn-lg">
                        {{ __('Login') }}
                    </button>
                    
                    <!-- /.btn btn-primary -->

                    <div class="mt-3 text-center text-capitalize">
                        <a href="#">forgot password?</a>
                    </div>
                    
                    <div class="mt-3">
                        Don’t have an account? <a href="{{ route('register') }}"> Sign up</a>
                    </div>
                    <!-- /.mt-2 -->
                </div>
                </form>
                <!-- /.login-form -->
            </div>
        </div>
    </div>
</div>
<!-- #login-modal .modal -->
<!-- END-OF Login Modal -->

<!-- START Register Modal -->
<div id="register-modal" class="modal" tabindex="-1" role="dialog">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content ">
            <div class="modal-body">

                <div class="register-form">
            <form method="POST" action="{{ route('register') }}" enctype="multipart/form-data">
                @csrf
                    <div class="form-group">
                    
                <input id="name" type="text" class="form-control form-control-lg @error('name') is-invalid @enderror" placeholder="First name" name="name" value="{{ old('name') }}" required autocomplete="name" autofocus>

                    @error('name')
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $message }}</strong>
                        </span>
                    @enderror
                        <!-- /.form-control -->
                    </div>
                    <!-- /.form-group -->
                    <div class="form-group">
                        <input id="phone" type="text" class="form-control form-control-lg @error('phone') is-invalid @enderror" placeholder="Phone Number" name="phone" value="{{ old('phone') }}" required autocomplete="phone" autofocus>

                    @error('phone')
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $message }}</strong>
                        </span>
                    @enderror
                        <!-- /.form-control -->
                    </div>
                    <!-- /.form-group -->

                    <div class="form-group">
                        <input id="email" type="email" class="form-control form-control-lg @error('email') is-invalid @enderror" placeholder="Email Address" name="email" value="{{ old('email') }}" required autocomplete="email">

                    @error('email')
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $message }}</strong>
                        </span>
                    @enderror
                        <!-- /.form-control -->
                    </div>
                    <!-- /.form-group -->

                    <div class="form-group">
                        <input id="password" type="password" class="form-control form-control-lg @error('password') is-invalid @enderror" placeholder="Password" name="password" required autocomplete="new-password">

                    @error('password')
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $message }}</strong>
                        </span>
                    @enderror
                        <!-- /.form-control -->
                    </div>
                    <!-- /.form-group -->

                    <div class="form-group">
                            <input id="password-confirm" type="password" class="form-control form-control-lg" placeholder="Comfirm Password" name="password_confirmation" required autocomplete="new-password">
                    </div>

                        <!-- /.form-control -->
                    </div>
                    <button type="submit" class="mt-4 btn btn-secondary btn-block btn-lg">
                        {{ __('Register') }}
                    </button>
                    <!-- /.mt-2 -->
                    <div class="mt-3">
                        Already have an account? <a href="{{ route('login') }}">Log in</a>
                    </div>
                    <!-- /.mt-2 -->
                </div>
            </form>
                <!-- /.login-form -->
            </div>
        </div>
    </div>
</div>
<!-- #register-modal .modal -->

@endsection