<header>
    <nav class="navbar navbar-light navbar-expand-md py-md-2 fixed-top">
        <div class="container">
            <a href="index.html" class="navbar-brand">
                <img src="{{asset('cliassets/images/logo.svg')}}" height="30" alt="logo" class="brand-logo"/> </a>
            <!-- /.navbar-brand -->
            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNav" aria-expanded="false" aria-label="Toggle navigation">
                <span class="navbar-toggler-icon"></span>
                <!-- /.navbar-toggler-icon -->
            </button>
            <!-- /.navbar-toggler -->
            
            <div class="navbar-collapse collapse justify-content-end" id="navbarNav">
                <ul class="navbar-nav ">
                                            
                @guest
                    <li class="nav-item  py-md-2">
                        <a class="navbar-brand" href="{{ route('login') }}"  data-toggle="modal" data-target="#login-modal">Login</a>
                    </li>
                    @if (Route::has('register'))
                        <li class="nav-item  py-md-2">
                            <a class="navbar-brand" href="{{ route('register') }}" data-toggle="modal" data-target="#register-modal">{{ __('Register') }}</a>
                        </li>
                    @endif
                @else
                    <li class="nav-item active">
                        <a href="{{ url('/') }}" class="navbar-brand ">Home</a>                               
                    </li>
                    <li class="nav-item dropdown">
                        <a class=" dropdown-toggle navbar-brand" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                Author  
                        </a>
                        <div class="dropdown-menu" aria-labelledby="navbarDropdown">
                        @foreach($authors as $au)
                            <a class="dropdown-item" href="#">{{$au->name }}</a>
                        @endforeach
                        </div>
                    </li>
                    <li class="nav-item">
                        <a href="{{ url('/') }}" class="navbar-brand">Blog</a>                               
                    </li>
                    <li class="nav-item">
                        <a href="{{ url('/') }}" class="navbar-brand">Conatct Us</a>                               
                    </li>   
                    <li class="nav-item dropdown py-md-2">
                        <a href="#" class="navbar-brand" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="true">{{ auth()->user()->name }}
                        <img src="{{asset('/uploads/' . auth()->user()->image )}}" alt="user profile img" class="user-profile-img"/></a>
                                
                        <div class="dropdown-menu">
                            <a class="dropdown-item" href="profile-edit.html">Edit Profile</a>
                            <a class="dropdown-item" href="{{ route('logout') }}"
                                onclick="event.preventDefault();
                                                document.getElementById('logout-form').submit();">
                                {{ __('Logout') }}
                            </a>

                            <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                @csrf
                            </form>
                        </div>	
                    </li>
                @endguest
                    
                        </div>
                        <!-- /.dropdown-menu -->
                    </li>
                    <!-- /.nav-item py-md-2 -->
                </ul>
                <!-- /.navbar-nav -->
            </div>
            <!-- /#navbarNav.navbar-collapse collapse -->
        </div>
        <!-- /.container -->
    </nav>
    <!-- /.navbar navbar-dark bg-primary navbar-expand-md py-md-2 -->
</header>