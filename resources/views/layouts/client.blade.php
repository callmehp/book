<!DOCTYPE HTML>
<html lang="zxx">
    
<!-- Mirrored from qbgrow.com/magen/travelgo/index.html by HTTrack Website Copier/3.x [XR&CO'2014], Wed, 04 Mar 2020 05:16:16 GMT -->
<head>
        <!-- REQUIRED meta tags -->
        <meta charset="utf-8"/>
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no"/>
        <!-- Fav icon tag -->
        <link rel="shortcut icon" href="http://qbgrow.com/favicon.ico" type="image/x-icon">
        <link rel="icon" href="http://qbgrow.com/favicon.ico" type="image/x-icon">
        <!-- SEO meta tags -->
        <meta name="keywords" content=""/>
        <meta name="description" content="Premium High Quality and Responsive - Travel and Tours listings HTML template."/>
        <meta name="author" content="RexQ"/>
        <!-- Page title -->
        <title>Home - Travelgo - Travel and Tours listings HTML template</title>
        <!-- :::::-[ Vendors StyleSheets ]-:::::: -->
        <link rel="stylesheet" href="{{asset('cliassets/vendors/bootstrap.4.1/css/bootstrap.min.css')}}"/>
        <link rel="stylesheet" href="{{asset('cliassets/css/owl.carousel.min.css')}}"/>
        <link rel="stylesheet" href="{{asset('cliassets/vendors/font-awesome/css/font-awesome.css')}}"/>
        <link rel="stylesheet" href="{{asset('cliassets/vendors/animate.css/animate.css')}}"/>
        <link rel="stylesheet" href="{{asset('cliassets/vendors/owl-carousel/assets/owl.carousel.min.css')}}"/>
        <link rel="stylesheet" href="{{asset('cliassets/vendors/owl-carousel/assets/owl.theme.default.min.css')}}"/>
        <link rel="stylesheet" href="{{asset('cliassets/vendors/flag-icon-css/css/flag-icon.min.css')}}"/>
        <link rel="stylesheet" href="{{asset('cliassets/vendors/flaticon/flaticon.css')}}"/>
        <link rel="stylesheet" href="{{asset('cliassets/vendors/hover-effects/effects.min.css')}}"/>
        <link rel="stylesheet" href="{{asset('cliassets/vendors/ion.rangeslider/css/ion.rangeSlider.css')}}"/>
        <link rel="stylesheet" href="{{asset('cliassets/vendors/ion.rangeslider/css/ion.rangeSlider.skinFlat.css')}}"/>
        <link rel="stylesheet" href="{{asset('cliassets/vendors/icheck/skins/square/aero.css')}}"/>

        <!-- :::::-[ Travelgo - Travel and Tours listings HTML template StyleSheet ]-:::::: -->
        <link rel="stylesheet" href="{{asset('cliassets/css/style.css')}}"/>
        <link rel="stylesheet" href="{{asset('cliassets/css/custom.css')}}"/>
    </head>
    <!-- START-OF Home Page Body Tag -->
    <body>
        <!-- ::::::-[ START PAGE MAIN HEADER ]-:::::: -->
        @yield('header')
        <!-- ::::::-[ END-OF PAGE MAIN HEADER ]-:::::: -->

        <!-- ::::::-[ START PAGE MAIN CONTENT ]-:::::: -->

        
        <main>

        @yield('content')
        
        </main>
        <!-- ::::::-[ END-OF PAGE MAIN CONTENT ]-:::::: -->
        <!-- ::::::-[ START PAGE FOOTER ]-:::::: -->
        <footer>
           
        </footer>
        <!-- ::::::-[ END-OF PAGE FOOTER ]-:::::: -->
        <!-- ::::::-[ Load Javascript Vendors ]-:::::: -->
        <script src="{{asset('cliassets/vendors/jquery/jquery.3.3.1.js')}}"></script>
        <script src="{{asset('cliassets/vendors/popper.js/popperjs.min.js')}}"></script>
        <script src="{{asset('cliassets/vendors/bootstrap.4.1/js/bootstrap.min.js')}}"></script>
        <script src="{{asset('cliassets/vendors/owl-carousel/owl.carousel.min.js')}}"></script>
        <script src="{{asset('cliassets/vendors/parallax/parallax-scroll.js')}}"></script>
        <script src="{{asset('cliassets/vendors/sticky/jquery.sticky-sidebar.js')}}"></script>
        <script src="{{asset('cliassets/vendors/sticky-kit/sticky-kit.js')}}"></script>
        <script src="{{asset('cliassets/vendors/ion.rangeslider/js/ion.rangeSlider.js')}}"></script>
        <script src="{{asset('cliassets/vendors/icheck/icheck.min.js')}}"></script>
        <script src="assets/vendors/countdown/jquery.countdown.min.js')}}"></script>
        <script src="https://maps.googleapis.com/maps/api/js?v=3.exp&amp;sensor=false"></script>
        <!-- ::::::-[ Travelgo - Travel and Tours listings HTML template Javascript ]-::::::   -->
        <script src="{{asset('cliassets/js/main.js')}}"></script>
        <script> (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){ (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o), m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m) })(window,document,'script','../../../www.google-analytics.com/analytics.js','ga'); ga('create', 'UA-83200266-1', 'auto'); ga('send', 'pageview'); </script>
  
    </body>
    <!-- END-OF Home Page Body Tag -->

<!-- Mirrored from qbgrow.com/magen/travelgo/index.html by HTTrack Website Copier/3.x [XR&CO'2014], Wed, 04 Mar 2020 05:21:34 GMT -->
</html>