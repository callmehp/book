<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class bookregister extends Model
{
    protected $table = 'bookregisters';
    protected $fillable = ['bookname','writtenby','filename','cover','category_id'];

    public function author()
    {
        return $this->belongsTo(Author::class,'writtenby','id');
    }

    public function category()
    {
        return $this->belongsTo(BookCategoryM::class,'category_id','id');
    }
}
