<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class BookCategoryM extends Model
{
	protected $table = 'book_category';
	protected $fillable = [
        'bookcategory'
    ];

    public function books()
    {
        return $this->hasMany(bookregister::class,'category_id','id');
    }
}
