<?php

namespace App\Http\Controllers;
use App\Author;
use Illuminate\Http\Request;

class AuthorsController extends Controller
{
 
    public function index()
    {
        $data = Author::all(); 
        return view('admin.bookauthor-view', compact('data'));
    }

   
    public function create()
    {
        return view('admin.bookauthor-edit');
    }

    public function store(Request $request)
    {
        $request->validate([
            'image' => 'required|mimes:jpg,png,jpeg',
            'name' => 'required'
        ]);

        if($request->hasFile('image')){
            $fileName = 'image_' . time().'.'.$request->file('image')->extension();
            $request->file('image')->move(public_path('uploads'), $fileName);
            $request->image = $fileName;
        }

        Author::create([
            "name" => $request->name, 
            "image" => $request->image
        ]);
        
        $request->session()->flash('msg','data entered sucessfully');
        return redirect()->route('authors.list');
    }

    
    public function edit($id)
    {
        $data = Author::find($id);
        //dd($data);
        return view('admin.bookauthor-edit',compact('data'));
    }

    
    public function update(Request $request, $id)
    {
        //dd($request->all());

        if($request->hasFile('image')){
            $fileName = 'image_' . time().'.'.$request->file('image')->extension();
            $request->file('image')->move(public_path('uploads'), $fileName);
            $request->image = $fileName;
        }
          $bookauthor = Author::find($id)->update([
             'name' => $request->name,
            'image'=> $request->image
         ]);
    
         
        return redirect()->route('author.list');
    }

    public function destroy(Author $Author,$id)
    {
        Author::destroy(array('id',$id));
		    return redirect()->route('author.list')->with('status','data deleted');
    }
}
