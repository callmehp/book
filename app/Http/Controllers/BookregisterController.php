<?php

namespace App\Http\Controllers;
use App\bookregister;
use App\Author;
use App\BookCategoryM;
use Illuminate\Http\Request;

class BookregisterController extends Controller
{
    
    public function index()
    {
        $c_rr = bookregister::with('author','category')->paginate(10);
        return view('admin.bookregister', compact('c_rr'));
    }

   
    public function create()
    {
        $authors = Author::all();
        $categories = BookCategoryM::all();
        return view('admin.book-edit', compact('authors','categories'));
    }

    public function store(Request $request)
    {
        $request->validate([
            'filename' => 'required|mimes:pdf,xlx,csv',
            'cover' => 'required|mimes:jpg,png,jpeg',
            'bookname' => 'required',
            'writtenby' => 'required',
        ]);
        dd($request->all());
        // // if validation fails
        // if($validator->fails()) {
        //     return back()->withErrors($validator->errors());
        // }
        if($request->hasFile('filename')){
            $fileName = 'file_' . time().'.'.$request->file('filename')->extension();
            $request->file('filename')->move(public_path('uploads'), $fileName);
            $request->filename = $fileName;
        }

        $request->cover = 'no_cover.png';
        if($request->hasFile('cover')){
            $coverName = 'cover_' . time().'.'.$request->file('cover')->extension();
            $request->file('cover')->move(public_path('uploads'), $coverName);
            $request->cover = $coverName;
        }

        bookregister::create([
            "bookname" => $request->bookname,
            "writtenby" => $request->writtenby,
            "filename" => $request->filename,
            "cover" => $request->cover,
            "category_id" => $request->category_id
        ]);
		
		$request->session()->flash('msg','data entered sucessfully');
		return redirect()->route('book.list');
    }

    public function edit(Request $request, $id)
    {
        // dd($id);
        $data = bookregister::find($id)->with('author','category');
        $authors = Author::all();
        $categories = BookCategoryM::all();
        return view('admin.book-edit',compact('data','authors','categories'));
    }

    public function update(Request $request,$id)
    {
        // dd($request->all());
        $book = bookregister::find($id)->update([
            'bookname' => $request->bookname,
            'writtenby' => $request->writtenby,
            'filename' => $request->filename,
        ]);
        return redirect()->route('book.list');
    }

 
    public function destroy(bookregister $bookregister,$id)
    {
        bookregister::destroy(array('id',$id));
		return redirect()->route('book.list')->with('status','data deleted');
    }
}
