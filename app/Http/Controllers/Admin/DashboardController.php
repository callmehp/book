<?php

namespace App\Http\Controllers\Admin;

use App\User;
use App\Author;
use App\bookregister;
use App\BookCategoryM;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class DashboardController extends Controller
{

    public function index(){
        $total = (object)[];

        $total->authors = Author::count();
        $total->categories = BookCategoryM::count();
        $total->books = bookregister::count();
        $total->users = User::where('usertype','!=','admin')->count();
        
        return view('admin.dashboard', compact('total'));
	}
    public function registered()
    {

    	$users = User::all();

    	return view('admin.register')->with('users',$users);

    }
    // here we create fuction for edit users
    public function registeredit(Request $request, $id)
    {
    	$users = User::findOrFail($id);
    	return view('admin.register-edit')->with('users',$users);
    }

    // here we create function for update button
    public function registerupdate(Request $request, $id)
    {
    	$users = User::find($id);
    	$users->name = $request->input('username');
    	$users->usertype = $request->input('usertype');
    	$users->update();

    	return redirect('/role-register')->with('status','data is updated'); 
    }
    //delete function
    public function registerdelete($id)
    {
        $users = User::findOrFail($id);
        $users->delete();

        return redirect('/role-register')->with('status','data deleted');

    }
}
