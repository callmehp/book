<?php

namespace App\Http\Controllers;
use App\bookregister;
use App\Author;
use App\BookCategoryM;
use Illuminate\Http\Request;

class HomeController extends Controller
{
    // public function __construct()
    // {
    //     $this->middleware('auth');
    // }

    
    public function index()
    {
        $categories = BookCategoryM::all();
        $authors = Author::all();
        $books = bookregister::with('author','category')->get();
        return view('welcome', compact('books','categories','authors'));
    }
}
