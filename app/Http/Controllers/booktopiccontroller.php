<?php

namespace App\Http\Controllers;
use App\BookTopicM;
use Illuminate\Http\Request;

class booktopiccontroller extends Controller
{
    
    public function index()
    {
		
		return view('admin.booktopic-view')->with('c_arr',BookTopicM::all());
    }

   
    public function create()
    {
		return view('admin.booktopic-edit');
    }

    
    public function store(Request $request)
    {
        $res = new BookTopicM;
		$res->topic=$request->input('topic');
		$res->save();	
		$request->session()->flash('msg','data entered sucessfully');
		return redirect()->route('booktopic.list');
    }
   
    public function edit($id)
    {
        $data = BookTopicM::find($id);
        // dd($data);
        return view('admin.booktopic-edit',compact('data'));
    }

    
    public function update(Request $request, $id)
    {
        $booktopic = BookTopicM::find($id)->update([
            'booktopic' => $request->booktopic
        ]);
        return redirect()->route('booktopic.list');
    }

   
    public function destroy(BookTopicM $BookTopicM,$id)
    {
        BookTopicM::destroy(array('id',$id));
		    return redirect()->route('booktopic.list')->with('status','data deleted');
    }
}
