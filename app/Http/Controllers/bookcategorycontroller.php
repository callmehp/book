<?php

namespace App\Http\Controllers;
use App\BookCategoryM;
use Illuminate\Http\Request;

class bookcategorycontroller extends Controller
{
   
    public function index()
    {
		
		return view('admin.bookcategory-view')->with('c_arr',BookCategoryM::all());
    }

   
    public function create()
    {
		return view('admin.bookcategory-edit');
    }

    
    public function store(Request $request)
    {
        $res = new BookCategoryM;
		$res->bookcategory=$request->input('bookcategory');
		$res->save();	
		$request->session()->flash('msg','data entered sucessfully');
		return redirect()->route('bookcategory.list');
    }
   
    public function edit($id)
    {
        $data = BookCategoryM::find($id);
        // dd($data);
        return view('admin.bookcategory-edit',compact('data'));
    }

    
    public function update(Request $request, $id)
    {
        $bookcategory = BookCategoryM::find($id)->update([
            'bookcategory' => $request->bookcategory
        ]);
        return redirect()->route('bookcategory.list');
    }

   
    public function destroy(BookCategoryM $BookCategoryM,$id)
    {
        BookCategoryM::destroy(array('id',$id));
		    return redirect()->route('bookcategory.list')->with('status','data deleted');
    }
}
