<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class BookTopicM extends Model
{
    protected $table = 'book_topic';
	protected $fillable = [
        'topic'
    ];
}
