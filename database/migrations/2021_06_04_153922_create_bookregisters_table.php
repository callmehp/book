<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateBookregistersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('bookregisters', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('bookname');
            $table->string('writtenby');
            $table->string('category_id');
            $table->string('filename');
            $table->string('cover')->default('no_cover.png');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('bookregisters');
    }
}
